<h2>Plugdj clone/music streaming app </h2>
<br/>
<p>Application was developed in an Agile team of 5 members using Python, Vue.js 3, Websockets</p>
<p>NOTE: this repository consists only of frontend part of the project because I was only responsible for that. <br/> As it is my portfolio project I am only showing my work.</p>
<p>Frameworks libraries in this project: Vue.js 3, VueX, Vuetify, Vue Router</p>
<p>Some of the project features:</p>
<ul>
<li>
Full authentication
</li>
<li>Main screen UI</li>
<img src="https://gitlab.com/tadasandr/plugdjclone/-/raw/main/src/assets/plug1.png">
<li>
Live chat
</li>
<li>
Live Queue
</li>
<li>
Live like/dislike
</li>
<img src="https://gitlab.com/tadasandr/plugdjclone/-/raw/main/src/assets/plug3.png">
<li>
Server/Playlists CRUD/Pop-up dialog
</li>
<img src="https://gitlab.com/tadasandr/plugdjclone/-/raw/main/src/assets/plug2.png">
</ul>